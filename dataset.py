
import torch
import torch.nn as nn
from torch.utils import data

import common as com

class Dataset(data.Dataset):
    
    """Characterizes a dataset for PyTorch"""

    def __init__(self, 
                 data, 
                 labels, 
                 n_vectors_ea_file=309):
                 
        """
        data: log-mel of all wav files
        labels: [0,0,0,...]

        """
#         self.files = files
        self.data = data
        self.labels = labels
        self.n_vectors_ea_file = n_vectors_ea_file
                 
#         self.mode = mode
#         self.msg = msg
#         self.n_mels = n_mels
#         self.n_frame = n_frames
#         self.n_hop_frames = n_hop_frames
#         self.n_fft = n_fft
#         self.hop_length = hop_length
#         self.power = power

    def __len__(self):
        """Denotes the total number of samples"""
        
        return int(self.data.shape[0]/self.n_vectors_ea_file)

    def __getitem__(self, index):
        """Generates one sample of data"""
        X = self.data[index * self.n_vectors_ea_file : (index + 1) * self.n_vectors_ea_file, :]
        label = self.labels[index]
                 
        return X, label