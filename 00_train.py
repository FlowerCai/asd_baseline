########################################################################
# import default libraries
########################################################################
import os
import sys
import gc
########################################################################


########################################################################
# import additional libraries
########################################################################
import torch
import random
import numpy as np
import scipy.stats
# from import
from dataset import Dataset
from torch.utils.data import DataLoader,random_split
from tqdm import tqdm

try:
    from sklearn.externals import joblib
except:
    import joblib
# original lib
import common as com
from keras_model import AutoEncoder
# import keras_model
########################################################################


########################################################################
# load parameter.yaml
########################################################################
param = com.yaml_load()


########################################################################


########################################################################
# visualizer
########################################################################
class visualizer(object):
    def __init__(self):
        import matplotlib.pyplot as plt
        self.plt = plt
        self.fig = self.plt.figure(figsize=(7, 5))
        self.plt.subplots_adjust(wspace=0.3, hspace=0.3)

    def loss_plot(self, loss, val_loss):
        """
        Plot loss curve.

        loss : list [ float ]
            training loss time series.
        val_loss : list [ float ]
            validation loss time series.

        return   : None
        """
        ax = self.fig.add_subplot(1, 1, 1)
        ax.cla()
        ax.plot(loss)
        ax.plot(val_loss)
        ax.set_title("Model loss")
        ax.set_xlabel("Epoch")
        ax.set_ylabel("Loss")
        ax.legend(["Train", "Validation"], loc="upper right")

    def save_figure(self, name):
        """
        Save figure.

        name : str
            save png file path.

        return : None
        """
        self.plt.savefig(name)


########################################################################


########################################################################
# get data from the list for file paths
########################################################################
def file_list_to_data(file_list,
                      msg="calc...",
                      n_mels=64,
                      n_frames=5,
                      n_hop_frames=1,
                      n_fft=1024,
                      hop_length=512,
                      power=2.0):
    """
    convert the file_list to a vector array.
    file_to_vector_array() is iterated, and the output vector array is concatenated.

    file_list : list [ str ]
        .wav filename list of dataset
    msg : str ( default = "calc..." )
        description for tqdm.
        this parameter will be input into "desc" param at tqdm.

    return : numpy.array( numpy.array( float ) )
        data for training (this function is not used for test.)
        * dataset.shape = (number of feature vectors, dimensions of feature vectors)
    """
    # calculate the number of dimensions
    dims = n_mels * n_frames

    # iterate file_to_vector_array()
    for idx in tqdm(range(len(file_list)), desc=msg):
        vectors = com.file_to_vectors(file_list[idx],
                                      n_mels=n_mels,
                                      n_frames=n_frames,
                                      n_fft=n_fft,
                                      hop_length=hop_length,
                                      power=power)
#         vectors = torch.from_numpy(vectors)
        vectors_ = vectors[:: n_hop_frames, :]
#         if torch.equal(vectors_, vectors):
#             print("yes")
#             sys.exit(-1)
        if idx == 0:
            data = np.zeros((len(file_list) * vectors.shape[0], dims), float)
#             print(data.shape)
#             sys.exit(-1)
        data[vectors.shape[0] * idx: vectors.shape[0] * (idx + 1), :] = vectors

    return data


########################################################################
# 设置随机数种子
########################################################################
def setup_seed(seed):
     torch.manual_seed(seed)
     torch.cuda.manual_seed_all(seed)
     np.random.seed(seed)
     random.seed(seed)
     torch.backends.cudnn.deterministic = True

########################################################################
# main 00_train.py
########################################################################
if __name__ == "__main__":
    # check mode
    # "development": mode == True
    # "evaluation": mode == False
    mode = com.command_line_chk()

    if mode is None:
        sys.exit(-1)

    setup_seed(10)

    # make output directory exist_ok=True 创建目录不报错（创建目录的时候如果已经存在就不报错。）
    os.makedirs(param["model_directory"], exist_ok=True)

    # initialize the visualizer
#     visualizer = visualizer()

    # load base_directory list
    dirs = com.select_dirs(param=param, mode=mode) #返回数据集下的所有目录，['/data/caishifang/data/DCASE2021Task2_Dataset/dev_data/ToyCar', ... '/data/caishifang/data/DCASE2021Task2_Dataset/dev_data/valve']

    # loop of the base directory
    for idx, target_dir in enumerate(dirs):
        print("\n===========================")
        print("[{idx}/{total}] {target_dir}".format(target_dir=target_dir, idx=idx + 1, total=len(dirs)))
        # set path
        machine_type = os.path.split(target_dir)[1]
        model_file_path = "{model}/model_{machine_type}.pth".format(model=param["model_directory"],
                                                                     machine_type=machine_type) #每一个机器类型有着不同的模型，存储在hdf5文件当中
        if os.path.exists(model_file_path):
            com.logger.info("model exists")
            continue #如果模型存在就不继续训练


#         history_img = "{model}/history_{machine_type}.png".format(model=param["model_directory"],
#                                                                   machine_type=machine_type)

        # pickle file for storing anomaly score distribution
        score_distr_file_path = "{model}/score_distr_{machine_type}.pkl".format(model=param["model_directory"],
                                                                                machine_type=machine_type)  #存储模型得到的预测分数

        # generate dataset
        print("============== DATASET_GENERATOR ==============")

        # get file list for all sections
        # all values of y_true are zero in training
        files, y_true = com.file_list_generator(target_dir=target_dir,
                                                section_name="*",
                                                dir_name="train",
                                                mode=mode)#得到数据列表以及对应的标签
        data = file_list_to_data(files,
                                 msg="generate train_dataset",
                                 n_mels=param["feature"]["n_mels"],
                                 n_frames=param["feature"]["n_frames"],
                                 n_hop_frames=param["feature"]["n_hop_frames"],
                                 n_fft=param["feature"]["n_fft"],
                                 hop_length=param["feature"]["hop_length"],
                                 power=param["feature"]["power"])#将数据转化成logmel谱

        # number of vectors for each wave file
        #data.shape=(929781,640), len(files)=3009 n_vectors_ea_file=309，是每个文件的特征向量长度(309,640)。
        n_vectors_ea_file = int(data.shape[0] / len(files))
        dataset = Dataset(data, y_true, n_vectors_ea_file=n_vectors_ea_file)
        dataset_size = len(dataset)
        valid_size = int(dataset_size * param["fit"]["validation_split"])
        train_size = dataset_size - valid_size
        train_dataset, test_dataset = random_split(dataset=dataset, lengths=[train_size, valid_size])

        train_dataloader = DataLoader(train_dataset, batch_size=param["fit"]["batch_size"], shuffle=param["fit"]["shuffle"], num_workers=param["fit"]["num_workers"])
        test_dataloader = DataLoader(test_dataset, batch_size=param["fit"]["batch_size"], shuffle=param["fit"]["shuffle"], num_workers=param["fit"]["num_workers"])
        dataloader_dict = {"train":train_dataloader, "valid":test_dataloader}

        device = torch.device('cuda') if torch.cuda.is_available() else torch.device('cpu')
        model = AutoEncoder(input_dim=640)
        model.to(device)
        optimizer = torch.optim.Adam(model.parameters(), lr=param["fit"]["lr"])
        loss_func = nn.MSELoss()

        epoch_train_loss = []
        epoch_valid_loss = []
        global_step = {'train': 0, 'valid': 0}
        # train model
        print("============== MODEL TRAINING ==============")
        for epoch in range(param["fit"]["epochs"]):

            # model training
            for phase in ['train', 'valid']:
                if phase == 'train':
                    model.train()
                else:
                    model.eval()

                for step, (data, _) in enumerate(tqdm(dataloader_dict[phase])):
                    sample_loss = {'train': 0.0, 'valid': 0.0}
                    for row in range(data.shape[0]):
                        x = data[row:]
                        optimizer.zero_grad()
                        with torch.set_grad_enabled(phase == "train"):
                            x = x.to(device)
                            decode = model(x)
                            # print(decode.shape)
                            loss = loss_func(decode, x)
                            # print(sample_loss)
                            if phase == 'train':
                                loss.backward()
                                optimizer.step()
                        sample_loss[phase] += loss.item()
                    if phase == 'train':
                        global_step[phase] += 1
                    else:
                        global_step[phase] += 1


       # calculate y_pred for fitting anomaly score distribution
        y_pred = []
        with torch.no_grad():
            for idx , (x, y) in enumerate(tqdm(DataLoader(dataset=dataset, shuffle=False, num_workers=param["fit"]["num_workers"]))):
                model.eval()
                x = x.to(device)
                decoded = model(x)
                anomaly_score = torch.mean(torch.square(decoded - x))


                #encoded, decoded = model(x)
                #anomaly_score = torch.mean(torch.square(decoded - x))
                # x_reconst, mu, log_var = model(x)
                # anomaly_score = torch.mean(torch.square(x_reconst - x))
                y_pred.append(anomaly_score.item())

        # fit anomaly score distribution
        shape_hat, loc_hat, scale_hat = scipy.stats.gamma.fit(y_pred)
        gamma_params = [shape_hat, loc_hat, scale_hat]
        joblib.dump(gamma_params, score_distr_file_path)

        torch.save(model.state_dict(), model_file_path)
        print(model_file_path)
        # logging.info("save_model -> {}".format(model_file_path))
        # logging.info("============== END TRAINING ==============")
